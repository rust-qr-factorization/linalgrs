# linalgrs

Basic BLAS,LAPACK-"like" functionality in pure rust. Currently bare minimum needed to support a sparse QR factorization.


This is experimental and will likely not follow standard BLAS or LAPACK interfaces. Rather will attempt to use Rust ownership semantics where possible to
avoid allocations, but I will not otherwise attempt _zero_ allocations within these functions.
