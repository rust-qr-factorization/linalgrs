extern crate test;

use num_traits::{Num};
use ndarray::{Array2,s};


pub fn matrix_multiply<F : Num + Copy + Clone>(a : &Array2<F>, b : &Array2<F>) -> Array2<F>{
    let sa = a.shape();
    let sb = b.shape();
    assert_eq!(sa[1],sb[0]);
    let m=sa[0];
    let n=sb[1];
    let mut out = Array2::<F>::zeros((m,n));

    for r in 0..m{
        for c in 0..n{
            let sla=a.slice(s![r,..]);
            let slb=b.slice(s![..,c]);
            out[[r,c]]=sla.iter().zip(slb.iter()).map(|(x1,x2)|{(x1.clone())*(x2.clone())}).fold((F::zero(),F::zero()),|(sum,z),x|{
                let y = x-z;
                let t = sum+y;
                (t,(t-sum)-y)
            }).0;
        }
    }
    out
}



#[cfg(test)]
mod tests {
    use core::fmt::Debug;
    use super::matrix_multiply;
    use super::test::Bencher;
    use num_traits::Float;
    use ndarray::Zip;
    use ndarray::{Array2};
    use ndarray_rand::RandomExt;
    use ndarray_rand::rand::SeedableRng;
    use ndarray_rand::rand_distr::Uniform;
    use rand_isaac::isaac64::Isaac64Rng;
    use num::NumCast;
    use num_complex::Complex;
    use num::Num;



    
    //Make sure matrix multiplication panics when shapes are not consistent
    fn bad_shape<F : Num + Clone + Copy>(){
        let m=50;
        let n=20;
        let k=23;
        let a = Array2::<F>::zeros((m,k+1));
        let b = Array2::<F>::zeros((k-1,n));
        let _c = matrix_multiply(&a,&b);
    }
    #[test]
    #[should_panic]
    fn bad_shape_f32() {
        bad_shape::<f32>();
    }
    #[test]
    #[should_panic]
    fn bad_shape_f64(){
        bad_shape::<f64>();
    }
    #[test]
    #[should_panic]
    fn bad_shape_c32(){
        bad_shape::<Complex<f32>>();
    }
    #[test]
    #[should_panic]
    fn bad_shape_c64(){
        bad_shape::<Complex<f64>>();
    }





    fn make_random_matrix<F : Float + NumCast>(m : usize, n : usize, rng : &mut Isaac64Rng) -> Array2<F> {
        let tmp = Array2::<f64>::random_using((m,n),Uniform::new(-1.0,1.0),rng);
        let mut b = Array2::<F>::zeros((m,n));
        for (u,v) in tmp.iter().zip(b.iter_mut()){
            *v = F::from(*u).expect("Couldn't convert from f64");
        }
        b
    }

    fn make_random_complex_matrix<F : Float + NumCast>(m : usize, n : usize, rng : &mut Isaac64Rng) -> Array2<Complex<F>> {
        let tmp_re = Array2::<f64>::random_using((m,n),Uniform::new(-1.0,1.0),rng);
        let tmp_im = Array2::<f64>::random_using((m,n),Uniform::new(-1.0,1.0),rng);
        let mut b = Array2::<Complex<F>>::zeros((m,n));
        for ((re,im),v) in tmp_re.iter().zip(tmp_im.iter()).zip(b.iter_mut()){
            *v = Complex::<F>::new( F::from(*re).unwrap(), F::from(*im).unwrap() );
        }
        b
    }




    //Make sure matrix multiplication is linear.
    fn numerical_linearity<F : Float + NumCast + Debug>(){
        let eps = F::epsilon();
        let scaling = F::from(8000.0).unwrap();
        let tol = eps*scaling;
        let m=50;
        let n=20;
        let k=23;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);

        let nsamples=1000;
        let mut errs = Vec::<F>::new();
        for _ in 0..nsamples{
            let alpha : F = F::from(-1.2345678).unwrap();
            let beta : F = F::from(8.129875).unwrap();
            let a = make_random_matrix::<F>(m,k,&mut rng);
            let b1 = make_random_matrix::<F>(k,n,&mut rng);
            let b2 = make_random_matrix::<F>(k,n,&mut rng);
            //Form b=alpha*b1+beta*b2
            let b = Zip::from(&b1).and(&b2).apply_collect(|&x,&y|{alpha*x+beta*y});
            //Form c=alpha*a*b1 + beta*a*b2
            let ab1 = matrix_multiply(&a,&b1);
            let ab2 = matrix_multiply(&a,&b2);
            let c = Zip::from(&ab1).and(&ab2).apply_collect(|&x,&y|{alpha*x+beta*y});
            let ab = matrix_multiply(&a,&b);

            let mut relerrs = Vec::<F>::new();
            //We should have c==ab
            for (x,y) in c.iter().zip(ab.iter()){
                let max =if x.abs()>y.abs() { x.abs() } else { y.abs() };
                let err=(*x-*y).abs();
                let relerr = err/max;
                relerrs.push(relerr);
            }
            relerrs.sort_by(|x,y| x.partial_cmp(y).unwrap());
            let max=relerrs.last().unwrap();
            errs.push(*max);
        }
        errs.sort_by(|x,y| x.partial_cmp(y).unwrap());
        let avg=errs.iter().fold(F::zero(),|acc,&x|acc+x) / F::from(errs.len()).unwrap();
        let med=errs[errs.len()/2];
        let max=errs.last().unwrap();
        let prob_num=errs.iter().map(|&x|{if x>tol {1} else {0}}).fold(0,|acc,x|{acc+x});
        let prob = F::from(prob_num).unwrap() / F::from(errs.len()).unwrap();
        print!("tol:      {:?},     average:   {:?},    median:    {:?},      max:    {:?}\n",tol,avg,med,max);
        print!("Prob(err>tol) = {:?}\n",prob);
        //Probability of catastrophic errors less than 5% on the uniformly distributed cases
        assert!(prob<F::from(0.05).unwrap());
    }

    fn numerical_linearity_complex<F : Float + NumCast + Debug>(){
        let eps = F::epsilon();
        let scaling = F::from(8000.0).unwrap();
        let tol = eps*scaling;
        let m=50;
        let n=20;
        let k=23;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);

        let nsamples=1000;
        let mut errs = Vec::<F>::new();
        for _ in 0..nsamples{
            let alpha  = Complex::<F>::new( F::from(0.12345).unwrap(),F::from(5.2823489).unwrap());
            let beta   = Complex::<F>::new( F::from(0.12345).unwrap(),F::from(5.2823489).unwrap());
            let a = make_random_complex_matrix::<F>(m,k,&mut rng);
            let b1 = make_random_complex_matrix::<F>(k,n,&mut rng);
            let b2 = make_random_complex_matrix::<F>(k,n,&mut rng);
            //Form b=alpha*b1+beta*b2
            let b = Zip::from(&b1).and(&b2).apply_collect(|&x,&y|{alpha*x+beta*y});
            //Form c=alpha*a*b1 + beta*a*b2
            let ab1 = matrix_multiply(&a,&b1);
            let ab2 = matrix_multiply(&a,&b2);
            let c = Zip::from(&ab1).and(&ab2).apply_collect(|&x,&y|{alpha*x+beta*y});
            let ab = matrix_multiply(&a,&b);

            let mut relerrs = Vec::<F>::new();
            //We should have c==ab
            for (x,y) in c.iter().zip(ab.iter()){
                let max =if x.norm()>y.norm() { x.norm() } else { y.norm() };
                let err=(*x-*y).norm();
                let relerr = err/max;
                relerrs.push(relerr);
            }
            relerrs.sort_by(|x,y| x.partial_cmp(y).unwrap());
            let max=relerrs.last().unwrap();
            errs.push(*max);
        }
        errs.sort_by(|x,y| x.partial_cmp(y).unwrap());
        let avg=errs.iter().fold(F::zero(),|acc,&x|acc+x) / F::from(errs.len()).unwrap();
        let med=errs[errs.len()/2];
        let max=errs.last().unwrap();
        let prob_num=errs.iter().map(|&x|{if x>tol {1} else {0}}).fold(0,|acc,x|{acc+x});
        let prob = F::from(prob_num).unwrap() / F::from(errs.len()).unwrap();
        print!("tol:      {:?},     average:   {:?},    median:    {:?},      max:    {:?}\n",tol,avg,med,max);
        print!("Prob(err>tol) = {:?}\n",prob);
        //Probability of catastrophic errors less than 5% on the uniformly distributed cases
        assert!(prob<F::from(0.05).unwrap());
    }


    //Make sure matrix multiplication is associative
    fn numerical_associativity<F : Float + NumCast + Debug>(){
        let eps = F::epsilon();
        let scaling = F::from(8000.0).unwrap();
        let tol = eps*scaling;
        let m=50;
        let n=20;
        let k=23;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);

        let nsamples=1000;
        let mut errs = Vec::<F>::new();
        for _ in 0..nsamples{
            let a = make_random_matrix::<F>(m,k,&mut rng);
            let b = make_random_matrix::<F>(k,n,&mut rng);
            let c = make_random_matrix::<F>(n,n,&mut rng);

            let ab = matrix_multiply(&a,&b);
            let abc = matrix_multiply(&ab,&c);

            let bc   = matrix_multiply(&b,&c);
            let abc2 = matrix_multiply(&a,&bc);


            let mut relerrs = Vec::<F>::new();
            //We should have c==ab
            for (x,y) in abc.iter().zip(abc2.iter()){
                let max =if x.abs()>y.abs() { x.abs() } else { y.abs() };
                let err=(*x-*y).abs();
                let relerr = err/max;
                relerrs.push(relerr);
            }
            relerrs.sort_by(|x,y| x.partial_cmp(y).unwrap());
            let max=relerrs.last().unwrap();
            errs.push(*max);
        }
        errs.sort_by(|x,y| x.partial_cmp(y).unwrap());
        let avg=errs.iter().fold(F::zero(),|acc,&x|acc+x) / F::from(errs.len()).unwrap();
        let med=errs[errs.len()/2];
        let max=errs.last().unwrap();
        let prob_num=errs.iter().map(|&x|{if x>tol {1} else {0}}).fold(0,|acc,x|{acc+x});
        let prob = F::from(prob_num).unwrap() / F::from(errs.len()).unwrap();
        print!("tol:      {:?},     average:   {:?},    median:    {:?},      max:    {:?}\n",tol,avg,med,max);
        print!("Prob(err>tol) = {:?}\n",prob);
        //Probability of catastrophic errors less than 5% on the uniformly distributed cases
        assert!(prob<F::from(0.05).unwrap());
    }

    //Make sure matrix multiplication is associative
    fn numerical_associativity_complex<F : Float + NumCast + Debug>(){
        let eps = F::epsilon();
        let scaling = F::from(8000.0).unwrap();
        let tol = eps*scaling;
        let m=50;
        let n=20;
        let k=23;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);

        let nsamples=1000;
        let mut errs = Vec::<F>::new();
        for _ in 0..nsamples{
            let a = make_random_complex_matrix::<F>(m,k,&mut rng);
            let b = make_random_complex_matrix::<F>(k,n,&mut rng);
            let c = make_random_complex_matrix::<F>(n,n,&mut rng);

            let ab = matrix_multiply(&a,&b);
            let abc = matrix_multiply(&ab,&c);

            let bc   = matrix_multiply(&b,&c);
            let abc2 = matrix_multiply(&a,&bc);


            let mut relerrs = Vec::<F>::new();
            //We should have c==ab
            for (x,y) in abc.iter().zip(abc2.iter()){
                let max =if x.norm()>y.norm() { x.norm() } else { y.norm() };
                let err=(*x-*y).norm();
                let relerr = err/max;
                relerrs.push(relerr);
            }
            relerrs.sort_by(|x,y| x.partial_cmp(y).unwrap());
            let max=relerrs.last().unwrap();
            errs.push(*max);
        }
        errs.sort_by(|x,y| x.partial_cmp(y).unwrap());
        let avg=errs.iter().fold(F::zero(),|acc,&x|acc+x) / F::from(errs.len()).unwrap();
        let med=errs[errs.len()/2];
        let max=errs.last().unwrap();
        let prob_num=errs.iter().map(|&x|{if x>tol {1} else {0}}).fold(0,|acc,x|{acc+x});
        let prob = F::from(prob_num).unwrap() / F::from(errs.len()).unwrap();
        print!("tol:      {:?},     average:   {:?},    median:    {:?},      max:    {:?}\n",tol,avg,med,max);
        print!("Prob(err>tol) = {:?}\n",prob);
        //Probability of catastrophic errors less than 5% on the uniformly distributed cases
        assert!(prob<F::from(0.05).unwrap());
    }



    #[test]
    fn numerical_linearity_f32(){
        numerical_linearity::<f32>();
    }

    #[test]
    fn numerical_linearity_f64(){
        numerical_linearity::<f64>();
    }

    #[test]
    fn numerical_linearity_c32(){
        numerical_linearity_complex::<f32>();
    }

    #[test]
    fn numerical_linearity_c64(){
        numerical_linearity_complex::<f64>();
    }




    #[test]
    fn numerical_associativity_f32(){
        numerical_associativity::<f32>();
    }

    #[test]
    fn numerical_associativity_f64(){
        numerical_associativity::<f64>();
    }

    #[test]
    fn numerical_associativity_c32(){
        numerical_associativity_complex::<f32>();
    }

    #[test]
    fn numerical_associativity_c64(){
        numerical_associativity_complex::<f64>();
    }










    #[bench]
    fn matrix_mutiply_square_100x100_f32(bench : &mut Bencher){
        type F=f32;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 100;
        let k = 100;
        let n = 100;
        let a = make_random_matrix::<F>(m,k,&mut rng);
        let b = make_random_matrix::<F>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_200x200_f32(bench : &mut Bencher){
        type F=f32;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 200;
        let n = 200;
        let a = make_random_matrix::<F>(m,k,&mut rng);
        let b = make_random_matrix::<F>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_400x400_f32(bench : &mut Bencher){
        type F=f32;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 400;
        let k = 400;
        let n = 400;
        let a = make_random_matrix::<F>(m,k,&mut rng);
        let b = make_random_matrix::<F>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_800x800_f32(bench : &mut Bencher){
        type F=f32;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 400;
        let k = 400;
        let n = 400;
        let a = make_random_matrix::<F>(m,k,&mut rng);
        let b = make_random_matrix::<F>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }


    #[bench]
    fn matrix_mutiply_square_100x100_f64(bench : &mut Bencher){
        type F=f64;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 100;
        let k = 100;
        let n = 100;
        let a = make_random_matrix::<F>(m,k,&mut rng);
        let b = make_random_matrix::<F>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_200x200_f64(bench : &mut Bencher){
        type F=f64;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 200;
        let n = 200;
        let a = make_random_matrix::<F>(m,k,&mut rng);
        let b = make_random_matrix::<F>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_400x400_f64(bench : &mut Bencher){
        type F=f64;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 400;
        let k = 400;
        let n = 400;
        let a = make_random_matrix::<F>(m,k,&mut rng);
        let b = make_random_matrix::<F>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_800x800_f64(bench : &mut Bencher){
        type F=f64;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 400;
        let k = 400;
        let n = 400;
        let a = make_random_matrix::<F>(m,k,&mut rng);
        let b = make_random_matrix::<F>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }





    #[bench]
    fn matrix_mutiply_square_100x100_c32(bench : &mut Bencher){
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 100;
        let k = 100;
        let n = 100;
        let a = make_random_complex_matrix::<f32>(m,k,&mut rng);
        let b = make_random_complex_matrix::<f32>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_200x200_c32(bench : &mut Bencher){
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 200;
        let n = 200;
        let a = make_random_complex_matrix::<f32>(m,k,&mut rng);
        let b = make_random_complex_matrix::<f32>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_400x400_c32(bench : &mut Bencher){
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 400;
        let k = 400;
        let n = 400;
        let a = make_random_complex_matrix::<f32>(m,k,&mut rng);
        let b = make_random_complex_matrix::<f32>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_800x800_c32(bench : &mut Bencher){
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 400;
        let k = 400;
        let n = 400;
        let a = make_random_complex_matrix::<f32>(m,k,&mut rng);
        let b = make_random_complex_matrix::<f32>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }


    #[bench]
    fn matrix_mutiply_square_100x100_c64(bench : &mut Bencher){
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 100;
        let k = 100;
        let n = 100;
        let a = make_random_complex_matrix::<f64>(m,k,&mut rng);
        let b = make_random_complex_matrix::<f64>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_200x200_c64(bench : &mut Bencher){
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 200;
        let n = 200;
        let a = make_random_complex_matrix::<f64>(m,k,&mut rng);
        let b = make_random_complex_matrix::<f64>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_400x400_c64(bench : &mut Bencher){
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 400;
        let k = 400;
        let n = 400;
        let a = make_random_complex_matrix::<f64>(m,k,&mut rng);
        let b = make_random_complex_matrix::<f64>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }

    #[bench]
    fn matrix_mutiply_square_800x800_c64(bench : &mut Bencher){
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 400;
        let k = 400;
        let n = 400;
        let a = make_random_complex_matrix::<f64>(m,k,&mut rng);
        let b = make_random_complex_matrix::<f64>(k,n,&mut rng);
        bench.iter(||{
            let _c = test::black_box(matrix_multiply(&a,&b));
       });
    }


}
