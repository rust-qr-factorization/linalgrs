#![feature(test)]

extern crate test;

pub mod matrix_multiply;
pub mod triangular_solve;



#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
