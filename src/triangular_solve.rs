extern crate test;
use num_traits::Num;
use ndarray::{Array2,ArrayView2,ArrayViewMut1,s};


fn upper_triangular_solve_column<F : Num + Clone + Copy>( r : &ArrayView2<F>, b : &mut ArrayViewMut1<F>){
    let sr = r.shape();
    let n=sr[0];
    for i in (0..n).rev(){
        for j in i+1..n{
            b[[i]]=b[[i]]-r[[i,j]]*b[[j]];
        }
        b[[i]]=b[[i]]/r[[i,i]];
    }
}

pub fn upper_triangular_solve<F : Num + Clone + Copy>( r : &ArrayView2<F>, b : &Array2<F>) -> Array2<F>{
    let sr = r.shape();
    let sb = b.shape();
    if sr[0] != sr[1]{
        panic!("Upper triangular matrix must be square");
    }
    if sr[1] != sb[0]{
        panic!("Dimension mismatch in triangular solve");
    }
    let n=sb[1];

    //Process b column-by-column
    let mut c = b.clone();
    for j in 0..n{
        let mut slb = c.slice_mut(s![..,j]);
        upper_triangular_solve_column::<F>(r,&mut slb);
    }
    c
}



#[cfg(test)]
mod tests {
    use super::upper_triangular_solve;
    use core::fmt::Debug;
    use super::test::Bencher; use num_traits::Float;
    use ndarray::{Array2};
    use ndarray_rand::RandomExt;
    use ndarray_rand::rand::SeedableRng;
    use ndarray_rand::rand_distr::Uniform;
    use rand_isaac::isaac64::Isaac64Rng;
    use num::NumCast;
    use num::Num;
    use num_complex::Complex;


    //Make sure matrix multiplication panics when shapes are not consistent
    fn bad_shape<F : Num + Clone + Copy>(){
        let m=50;
        let a = Array2::<F>::zeros((m,m));
        let b = Array2::<F>::zeros((m-1,m));
        let _out=upper_triangular_solve(&a.view(),&b);
    }
    #[test]
    #[should_panic]
    fn bad_shape_f32() {
        bad_shape::<f32>();
    }
    #[test]
    #[should_panic]
    fn bad_shape_f64(){
        bad_shape::<f64>();
    }

    #[test]
    #[should_panic]
    fn bad_shape_c32() {
        bad_shape::<Complex<f32>>();
    }
    #[test]
    #[should_panic]
    fn bad_shape_c64(){
        bad_shape::<Complex<f64>>();
    }
 
    


    fn make_random_matrix<F : Float + NumCast>(m : usize, n : usize, rng : &mut Isaac64Rng) -> Array2<F> {
        let tmp = Array2::<f64>::random_using((m,n),Uniform::new(-1.0,1.0),rng);
        let mut b = Array2::<F>::zeros((m,n));
        for (u,v) in tmp.iter().zip(b.iter_mut()){
            *v = F::from(*u).expect("Couldn't convert from f64");
        }
        b
    }

    fn make_random_complex_matrix<F : Float + NumCast>(m : usize, n : usize, rng : &mut Isaac64Rng) -> Array2<Complex<F>> {
        let tmp_re = Array2::<f64>::random_using((m,n),Uniform::new(-1.0,1.0),rng);
        let tmp_im = Array2::<f64>::random_using((m,n),Uniform::new(-1.0,1.0),rng);
        let mut b = Array2::<Complex<F>>::zeros((m,n));
        for ((re,im),v) in tmp_re.iter().zip(tmp_im.iter()).zip(b.iter_mut()){
            *v = Complex::<F>::new( F::from(*re).unwrap(), F::from(*im).unwrap() );
        }
        b
    }



    fn make_random_upper_triangular_matrix<F : Float + NumCast>(m : usize, n : usize,rng : &mut Isaac64Rng) -> Array2<F> {
        let tmp = Array2::<f64>::random_using((m,n),Uniform::new(-1.0,1.0),rng);
        let mut b = Array2::<F>::zeros((m,n));

        for (u,((r,c),v)) in tmp.iter().zip(b.indexed_iter_mut()){
            if c>r{
                *v = F::from(*u).unwrap();
            }
            if c==r{
                *v=F::from(10.0 * (*u).abs() ).unwrap();
            }
        }
        b
    }
    fn make_random_complex_upper_triangular_matrix<F : Float + NumCast>(m : usize, n : usize, rng : &mut Isaac64Rng) -> Array2<Complex<F>> {
        let tmp_re = Array2::<f64>::random_using((m,n),Uniform::new(-1.0,1.0),rng);
        let tmp_im = Array2::<f64>::random_using((m,n),Uniform::new(-1.0,1.0),rng);
        let mut b = Array2::<Complex<F>>::zeros((m,n));
        for ((((r,c),v),re),im) in b.indexed_iter_mut().zip(tmp_re.iter()).zip(tmp_im.iter()){
            if c>r{
                *v = Complex::<F>::new(F::from(*re).unwrap(), F::from(*im).unwrap() );
            }
            if c==r{
                *v = Complex::<F>::new(F::from( (*re)*15.0 ).unwrap(), F::from( (*im)*16.0 ).unwrap() );
            }

        }
        b
    }



    //Test that solving a triangular matrix with itself as input results in identity matrix
    fn test_solve_inverse<F : Float + NumCast + Debug>(){
        let eps = F::epsilon();
        let scaling = F::from(8000.0).unwrap();
        let tol = eps*scaling;
        let m=50;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let nsamples=1000;
        let mut errs = Vec::<F>::new();
        for _ in 0..nsamples{
            let a = make_random_upper_triangular_matrix::<F>(m,m,&mut rng);
            let b = a.clone();

            let out=upper_triangular_solve(&a.view(),&b);


            let mut relerrs = Vec::<F>::new();
            //We should have id==inv(A)*A
            for ((r,c),x) in out.indexed_iter(){
                if c==r{
                    let y=F::one();
                    let err=(*x-y).abs();
                    let relerr = err;
                    relerrs.push(relerr);
                }
                else{
                    let err=(*x).abs();
                    relerrs.push(err);
                }
            }
            relerrs.sort_by(|x,y| x.partial_cmp(y).unwrap());
            let max=relerrs.last().unwrap();
            errs.push(*max);
        }
        errs.sort_by(|x,y| x.partial_cmp(y).unwrap());
        let avg=errs.iter().fold(F::zero(),|acc,&x|acc+x) / F::from(errs.len()).unwrap();
        let med=errs[errs.len()/2];
        let max=errs.last().unwrap();
        let prob_num=errs.iter().map(|&x|{if x>tol {1} else {0}}).fold(0,|acc,x|{acc+x});
        let prob = F::from(prob_num).unwrap() / F::from(errs.len()).unwrap();
        print!("tol:      {:?},     average:   {:?},    median:    {:?},      max:    {:?}\n",tol,avg,med,max);
        print!("Prob(err>tol) = {:?}\n",prob);
        //Probability of catastrophic errors less than 5% on the uniformly distributed cases
        assert!(prob<F::from(0.05).unwrap());
    }

    fn test_solve_inverse_complex<F : Float + NumCast + Debug>(){
        let eps = F::epsilon();
        let scaling = F::from(8000.0).unwrap();
        let tol = eps*scaling;
        let m=50;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let nsamples=1000;
        let mut errs = Vec::<F>::new();
        for _ in 0..nsamples{
            let a = make_random_complex_upper_triangular_matrix::<F>(m,m,&mut rng);
            let b = a.clone();

            let out=upper_triangular_solve(&a.view(),&b);


            let mut relerrs = Vec::<F>::new();
            //We should have id==inv(A)*A
            for ((r,c),x) in out.indexed_iter(){
                if c==r{
                    let y=F::one();
                    let err=(*x-y).norm();
                    let relerr = err;
                    relerrs.push(relerr);
                }
                else{
                    let err=(*x).norm();
                    relerrs.push(err);
                }
            }
            relerrs.sort_by(|x,y| x.partial_cmp(y).unwrap());
            let max=relerrs.last().unwrap();
            errs.push(*max);
        }
        errs.sort_by(|x,y| x.partial_cmp(y).unwrap());
        let avg=errs.iter().fold(F::zero(),|acc,&x|acc+x) / F::from(errs.len()).unwrap();
        let med=errs[errs.len()/2];
        let max=errs.last().unwrap();
        let prob_num=errs.iter().map(|&x|{if x>tol {1} else {0}}).fold(0,|acc,x|{acc+x});
        let prob = F::from(prob_num).unwrap() / F::from(errs.len()).unwrap();
        print!("tol:      {:?},     average:   {:?},    median:    {:?},      max:    {:?}\n",tol,avg,med,max);
        print!("Prob(err>tol) = {:?}\n",prob);
        //Probability of catastrophic errors less than 5% on the uniformly distributed cases
        assert!(prob<F::from(0.05).unwrap());
    }


    #[test]
    fn test_solve_inverse_f32(){
        test_solve_inverse::<f32>();
    }

    #[test]
    fn test_solve_inverse_f64(){
        test_solve_inverse::<f64>();
    }

    #[test]
    fn test_solve_inverse_c32(){
        test_solve_inverse_complex::<f32>();
    }

    #[test]
    fn test_solve_inverse_c64(){
        test_solve_inverse_complex::<f64>();
    }




    #[bench]
    fn upper_triangular_solve_100x100_f32(bench : &mut Bencher){
        type F=f32;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 100;
        let k = 20;
        let a = make_random_upper_triangular_matrix::<F>(m,m,&mut rng);
        let b = make_random_matrix::<F>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_200x200_f32(bench : &mut Bencher){
        type F=f32;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_upper_triangular_matrix::<F>(m,m,&mut rng);
        let b = make_random_matrix::<F>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_400x400_f32(bench : &mut Bencher){
        type F=f32;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_upper_triangular_matrix::<F>(m,m,&mut rng);
        let b = make_random_matrix::<F>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_800x800_f32(bench : &mut Bencher){
        type F=f32;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_upper_triangular_matrix::<F>(m,m,&mut rng);
        let b = make_random_matrix::<F>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }




    #[bench]
    fn upper_triangular_solve_100x100_f64(bench : &mut Bencher){
        type F=f64;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 100;
        let k = 20;
        let a = make_random_upper_triangular_matrix::<F>(m,m,&mut rng);
        let b = make_random_matrix::<F>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_200x200_f64(bench : &mut Bencher){
        type F=f64;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_upper_triangular_matrix::<F>(m,m,&mut rng);
        let b = make_random_matrix::<F>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_400x400_f64(bench : &mut Bencher){
        type F=f64;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_upper_triangular_matrix::<F>(m,m,&mut rng);
        let b = make_random_matrix::<F>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_800x800_f64(bench : &mut Bencher){
        type F=f64;
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_upper_triangular_matrix::<F>(m,m,&mut rng);
        let b = make_random_matrix::<F>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }



    #[bench]
    fn upper_triangular_solve_100x100_c32(bench : &mut Bencher){
        
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 100;
        let k = 20;
        let a = make_random_complex_upper_triangular_matrix::<f32>(m,m,&mut rng);
        let b = make_random_complex_matrix::<f32>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_200x200_c32(bench : &mut Bencher){
        
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_complex_upper_triangular_matrix::<f32>(m,m,&mut rng);
        let b = make_random_complex_matrix::<f32>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_400x400_c32(bench : &mut Bencher){
        
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_complex_upper_triangular_matrix::<f32>(m,m,&mut rng);
        let b = make_random_complex_matrix::<f32>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_800x800_c32(bench : &mut Bencher){
        
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_complex_upper_triangular_matrix::<f32>(m,m,&mut rng);
        let b = make_random_complex_matrix::<f32>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }




    #[bench]
    fn upper_triangular_solve_100x100_c64(bench : &mut Bencher){
        
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 100;
        let k = 20;
        let a = make_random_complex_upper_triangular_matrix::<f64>(m,m,&mut rng);
        let b = make_random_complex_matrix::<f64>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_200x200_c64(bench : &mut Bencher){
        
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_complex_upper_triangular_matrix::<f64>(m,m,&mut rng);
        let b = make_random_complex_matrix::<f64>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_400x400_c64(bench : &mut Bencher){
        
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_complex_upper_triangular_matrix::<f64>(m,m,&mut rng);
        let b = make_random_complex_matrix::<f64>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

    #[bench]
    fn upper_triangular_solve_800x800_c64(bench : &mut Bencher){
        
        let seed = 219479;
        let mut rng = Isaac64Rng::seed_from_u64(seed);
        let m = 200;
        let k = 20;
        let a = make_random_complex_upper_triangular_matrix::<f64>(m,m,&mut rng);
        let b = make_random_complex_matrix::<f64>(m,k,&mut rng);
        bench.iter(||{
            let _c = test::black_box(upper_triangular_solve(&a.view(),&b));
       });
    }

}
